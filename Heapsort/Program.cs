﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
namespace Heapsort
{
    internal class Program
    {
        static public void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            Console.InputEncoding = System.Text.Encoding.UTF8;
            System.Console.InputEncoding = Encoding.GetEncoding(1251);
            int n; bool check;
            do
            {
                do
                {
                    Console.Write("Введіть розмір масиву: ");
                    check = int.TryParse(Console.ReadLine(), out n);
                } while (!check);

               TimeSpan count = CountSort(n);
               TimeSpan heap = HeapSorting(n);
               TimeSpan shell = ShellSort(n);
            


                //Console.Write($"Кількість елементів - {n}\nCount Sort = {count.TotalMilliseconds}\nHeap Sort = {heap.TotalMilliseconds}\nShell Sort = {shell.TotalMilliseconds}\n");
                Console.ReadLine();
                
            } while (true);
            
        }

        static public TimeSpan HeapSorting(int n)
        {
            int a = -20, b = 100;
            Random rnd = new Random();
            Stopwatch clock = new Stopwatch();
            
            double[] arr = new double[n];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = Math.Round(rnd.NextDouble() * (b - a) + a, 3);
            Console.WriteLine("Початковий масив:");
            for (int i = 0; i < arr.Length; i++)
            {
                if (i == arr.Length - 1) Console.Write($"{arr[i]}\n");
                else Console.Write($"{arr[i]}, ");
            }
            clock.Start();
            heapSort(arr);
            clock.Stop();
            Console.WriteLine("Відсортований масив:");
            for (int i = 0; i < arr.Length; i++)
            {
                if (i == arr.Length - 1) Console.Write($"{arr[i]}\n");
                else Console.Write($"{arr[i]}, ");
            }
            TimeSpan timestop = clock.Elapsed;
            return timestop; 
            
        }
        static void heapSort(double[] arr)
        {
            int n = arr.Length;
            for (int i = n / 2 - 1; i >= 0; i--)//вхідний масив перезбирається в max-кучу, перебираючи в першому циклі його елементи з кінця, за допомогою методу heap
                heap(arr, n, i);
            for (int i = n - 1; i >= 0; i--) //у другому циклі один за одним розглядаємо вузли кучі
            {
                double temp = arr[0];
                arr[0] = arr[i];
                arr[i] = temp;
                heap(arr, i, 0); //перебираємо зменшену на 1 кучу
            }
        }
        static void heap(double[] arr, int n, int i)//розглядає елемент масиву і створює двійкову кучу
        {
            int largest = i;
            int first = 2 * i + 1;// Він знаходить для нього лівого(arr[2 * i + 1])
            int second = 2 * i + 2;//та правого нащадка(arr[2 * i + 2])
            if (first < n && arr[first] > arr[largest])// отриманому піддереві шукається вузол з максимальним значенням
                largest = first;
            if (second < n && arr[second] > arr[largest])
                largest = second;
            if (largest != i)
            {
                double tmp = arr[i];
                arr[i] = arr[largest];
                arr[largest] = tmp;
                heap(arr, n, largest); //Якщо цей вузол не корінь, відкидаємо його значення зі  значенням кореневого вузла і знову рекурсивно викликаємо метод heap
            }//тоді створюємо дерево з кореневим вузло тим що працювали і т.д.

        }

        static TimeSpan CountSort(int n)
        {

            int a = -15, b = -10;
            Random rnd = new Random();
            Stopwatch clock = new Stopwatch();
           
            sbyte[] arr = new sbyte[n];
            for (int i = 0; i < arr.Length; i++)
                arr[i] = (sbyte)rnd.Next(a,b);
            Console.WriteLine("Початковий масив");
            for (int i = 0; i < arr.Length; i++)
            {
                if (i == arr.Length - 1) Console.Write($"{arr[i]}\n");
                else Console.Write($"{arr[i]}, ");
            }
            clock.Start();
            arr = counting(arr, arr.Min(), arr.Max());//Для методу підрахунку спочатку треба знайти мінімальне та максимальне значення масиву(користуюсь простором імен System.Linq)
            clock.Stop();
            TimeSpan timestop = clock.Elapsed;
            Console.WriteLine("Відсортований масив:");
            for (int i = 0; i < arr.Length; i++)
            {
                if (i == arr.Length - 1) Console.Write($"{(sbyte)arr[i]}\n");
                else Console.Write($"{arr[i]}, ");
            }

            return timestop; 
        }
        private static sbyte[] counting(sbyte[] arr, int min, int max)
        {
            int[] sortarr = new int[max - min + 1];//створюємо допоміжний масив розміром в різницю мінімального та максимального значення +1 початкового масиву, який заповнюємо нулями
            int j = 0;
            for (int i = 0; i < sortarr.Length; i++)
                sortarr[i] = 0;
            for (int i = 0; i < arr.Length; i++)//в залежності від значення елемента початкового масиву, у допоміжному масиву циклічно збільшуватиметься елемент
                sortarr[arr[i] - min]++;//що відповідає елементу початкового масиву на 1
            //у допоміжному масиві значення кожного i-того елемента будуть вказувати, скільки разів в початковому масиві зустрічається відповідне значення між max та min
            for (int i = min; i <= max; i++)//В останному циклі перезбирається початковий масив і заповнюється за допомогою допоміжного
                while (sortarr[i - min]-- > 0)
                {
                    arr[j] = (sbyte)i;
                    j++;
                }
            return arr;
        }

        static TimeSpan ShellSort(int n)
        {
            Console.InputEncoding = Encoding.Unicode;
            Console.OutputEncoding = Encoding.Unicode;
            Random rnd = new Random();
            Stopwatch clock = new Stopwatch();
            
            float[] arr = new float[n];
            for (int i = 0; i < arr.Length; i++)
            arr[i] = (float)(rnd.Next(0, 200) + Math.Round(rnd.NextDouble(),2));
            Console.WriteLine("Початковий масив");
            for (int i = 0; i < arr.Length; i++)
            {
                if (i == arr.Length - 1) Console.Write($"{arr[i]}\n");
                else Console.Write($"{arr[i]}, ");
            }
            int s = 0;
            while (((int)Math.Pow(2, s) + 1) / 2 <= arr.Length)//У першому циклі шукається таке набільше s, що задовільнить умову(Math.Pow(2, s) + 1)/ 2 <= arr.Lenght
            s++;
            int step = ((int)Math.Pow(2, s) + 1) / 2;//step
            float c;
            clock.Start();
            while (s >= -1)//цикл, що виконуватиметься поки s буде більшим або рівним за -1
            {
                for (int i = 0; i < (arr.Length - step); i++)
                {
                    int j = i;
                    while (j >= 0 && arr[j] > arr[j + step])
                    {
                        c = arr[j];//проходимося по його елементам і міняємо значення елемента arr[j] та arr[j + step]
                        arr[j] = arr[j + step];//за умови, що arr[j] більший, ніж arr[j+step].
                        arr[j + step] = c;
                        j -= step;
                    }
                }
                s--;//s зменшується на одиницю і треба знову розрахувати вже меншний крок step, цикл знову запуститься
                step = ((int)Math.Pow(2, s) + 1) / 2;//Це потрібно для того, щоб step звівся до нуля і останнім кроком step була одиниця
            }
            clock.Stop();
            Console.WriteLine("Відсортований масив:");
            for (int i = 0; i < arr.Length; i++)
            {
                if (i == arr.Length - 1) Console.Write($"{arr[i]}\n");
                else Console.Write($"{arr[i]}, ");
            }
            TimeSpan timestop = clock.Elapsed;
            return timestop; 
        }
    }
}

